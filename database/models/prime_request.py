from database.db import db
import datetime

class PrimeRequestHistoryRecord(db.Document):
    prime_member = db.IntField(required=True)
    prime_value = db.IntField(default=None)
    response_hostname = db.StringField(default=None)
    requested_at = db.DateTimeField(default=datetime.datetime.utcnow)