# HAPrime Project

This is the first part of the project HAPrime, where we will look into the workings of HAProxy software with Flask API powered by MongoDB via Mongo Atlas and how it works with Gitlab CI/CD pipeline jobs that will deploy to three staging servers.

1. Flask API
2. Heroku
3. Gitlab with CI/CD
4. Mongo Atlas



Before running docker-compose commands:
    - Setup virtual environment (conda has been used for this project)
    - pip install -r requirements.txt
    - Setup .env file containing environment variables:
        - FLASK_ENV -> Can be development, production or testing
        - TESTING_MONGODB_HOST -> Connection string for mongodb
        - STAGE_MONGODB_HOST
        - DEV_MONGODB_HOST
        - JWT_SECRET_KEY -> Used for authentification, for now it is not used anywhere, but in the next few iterations it will be used

- Get Docker containers up and running with scaling: docker-compose up -d --scale pyapp=N
- Check status of docker containers via docker-compose: docker-comnpose ps
- Stop all containers: docker-compose stop
- Remove all containers: docker-compose rm (after all services have been stopped)


## Next steps

- Build a Dockerfile for building and scaling multiple Clickhouse instances for distributed database
- Project reorganization
- Fix 