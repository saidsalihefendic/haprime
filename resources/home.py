from flask_restful import Resource


class HomeApi(Resource):
    def get(self):
        return {
            "content": "This is the HAPrime!",
            "author": "Said Salihefendic"
        }, 200
