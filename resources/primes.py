from flask import request
from urllib.parse import urlparse
from flask_restful import Resource
import math
import time
from database.models.prime_request import PrimeRequestHistoryRecord
import socket

found_primes = [2, 3]
def find_nth_prime(n):
    start = time.time()
    if len(found_primes) < n:
        current_number = found_primes[-1]
        while len(found_primes) != n:
            current_number = current_number + 2

            # assume the current number is prime
            is_prime = True
            for prime in found_primes:
                if current_number % prime == 0:
                    is_prime = False
                    break
                elif prime * prime > current_number:
                    break

            if is_prime:
                found_primes.append(current_number)
    
    return found_primes[n - 1]

class PrimesApi(Resource):
    def get(self, prime_member: int):
        if prime_member <= 1000000:
            prime_value = find_nth_prime(prime_member)
            hostname = socket.gethostname()

            payload = {
                "prime_member": prime_member,
                "prime_value": prime_value,
                "response_hostname": hostname 
            }

            prime_request_history_record = PrimeRequestHistoryRecord(**payload)

            prime_request_history_record.save()

            return payload, 200

        return {
            "msg": "N is too large for deterministic algorithm"
        }, 400