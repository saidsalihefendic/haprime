from .home import HomeApi
from .primes import PrimesApi

def initialize_routes(api):
    api.add_resource(HomeApi, "/api/home")
    api.add_resource(PrimesApi, "/api/primes/<int:prime_member>")
