import json

from tests.BaseCase import BaseCase

class TestPrimeRequest(BaseCase):
    def test_correct_prime(self):
        response = self.app.get(
            '/api/primes/100',
            headers={"Content-Type": "application/json"}
        )

        self.assertEqual(int, type(response.json["prime_member"]))
        self.assertEqual(int, type(response.json["prime_value"]))
        self.assertEqual(200, response.status_code)
        self.assertEqual(541, response.json["prime_value"])

    def test_invalid_member(self):
        response = self.app.get(
            '/api/primes/1000001',
            headers={"Content-Type": "application/json"}
        )

        self.assertEqual("N is too large for deterministic algorithm", response.json["msg"])
        self.assertEqual(400, response.status_code)