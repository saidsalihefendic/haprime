from flask import Flask, request
from flask_restful import Api
from flask_cors import CORS
from constants import DEVELOPMENT, STAGE, TESTING
from os import environ
from database.db import initialize_db
import socket

from os import environ, path
from dotenv import load_dotenv

basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, '.env'))

mode = environ.get("FLASK_ENV")
app = Flask(__name__)

if mode == DEVELOPMENT:
    app.config.from_object("config.DevConfig")
elif mode == STAGE:
    app.config.from_object("config.StageConfig")
elif mode == TESTING:
    app.config.from_object("config.TestConfig")

@app.route("/")
def is_running():
    return "The api is up and running! Running on {}3.".format(request.host)


CORS(app)
api = Api(app)
initialize_db(app)

from resources.routes import initialize_routes
initialize_routes(api)