from os import environ, path
from dotenv import load_dotenv

basedir = path.abspath(path.dirname(__file__))
load_dotenv(path.join(basedir, '.env'))

class Config:
    """Base config."""
    JWT_SECRET_KEY = environ.get('JWT_SECRET_KEY')
    # STATIC_FOLDER = 'static'
    # TEMPLATES_FOLDER = 'templates'


class StageConfig(Config):
    FLASK_ENV = 'production'
    DEBUG = False
    TESTING = False
    MONGODB_SETTINGS = {
        "host": environ.get('STAGE_MONGODB_HOST')
    }


class TestConfig(Config):
    DEBUG = False
    TESTING = True
    MONGODB_SETTINGS = {
        "host": environ.get('TESTING_MONGODB_HOST')
    }


class DevConfig(Config):
    DEBUG = True
    TESTING = True
    MONGODB_SETTINGS = {
        "host": environ.get('DEV_MONGODB_HOST')
    }