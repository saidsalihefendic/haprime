FROM ubuntu:20.04
RUN apt-get update \
    && apt-get install -y python3-pip \
    && apt-get install -y git
WORKDIR /myapp
RUN git clone https://gitlab.com/saidsalihefendic/haprime.git .
RUN pip install -r requirements.txt
CMD python3 run.py
EXPOSE 5000